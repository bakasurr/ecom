import React, { useState, useEffect } from "react";
import axios from "axios";

export default () => {

    const [products, setProducts] = useState([]);

    const fetchProducts = async() => {
        const res = await axios.get('http://localhost:4000/products');
        setProducts(res.data.products);
    };

    useEffect(() => {
        fetchProducts();
    }, []);

    const addtoCartClickHandler = async(product, event) => {
        event.preventDefault();
        const cartItem = {...product};
        await axios.post('http://localhost:4000/carts/', {
            cartItem
        });
    }

    const renderedProducts = Object.values(products).map(product => {
        return (
            <div className="col-md-4" key={product.id}>
                <div className="card">
                    <img className="card-img-top" src={product.img} alt={product.name} style={{width: '50%', margin: '2% 16%'}}></img>
                    <div className="card-body">
                    <h5 className="card-title border-bottom pb-3">{product.name} </h5>
                    <small className="text-muted">{product.price} rs</small>
                    <a href="#" className="float-right" onClick={addtoCartClickHandler.bind(this, product)}>Add to Cart <i className="fas fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        )
    });

    return (
        <div className="row">
                {renderedProducts}
        </div>
    )
}
