import React, { useState, useEffect } from "react";
import axios from "axios";

export default () => {

    const [carts, setCarts] = useState([]);

    const fetchCarts = async() => {
        const res = await axios.get('http://localhost:4000/carts');
        setCarts(res.data.carts);
    };

    useEffect(() => {
        fetchCarts();
    }, []);

    const divStyle = {
        maxWidth: '540px'
      };

    const renderedCartItems = Object.values(carts).map(item => {
        return (
            <div className="col-lg-12 p-3 cardlist" key={item.id}>
                <div className="col-lg-12">
                    <div className="row">
                        <div className="col-lg-8">
                            <div className="row">
                                <div className="col-4 col-lg-3 col-xl-2">
                                    <div className="row">
                                        <a href="cateview.php" className="w-100">
                                            <img src={item.img} alt={item.name} className="mx-auto d-block mb-1 addcartimg" width='94%'></img>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-8 col-lg-9 col-xl-10">
                                    <div className="d-block text-truncate mb-1" style={{marginLeft: '5%'}}>
                                        <a href="cateview.php" className="cartproname">{item.name}</a>
                                    </div>
                                    <div className="cartviewprice d-block" style={{marginLeft: '5%'}}>
                                        <span className="amt">Rs.{item.price}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    });


    return (
        <div className="col-lg-9 px-0 pr-lg-2 mb-2 mb-lg-0">
            <div className="card border-light bg-white card proviewcard shadow-sm">
                <div className="card-header">My Cart</div>
                    <div className="card-body">
                        {renderedCartItems}
                </div>
            </div>
        </div>
    )
}
