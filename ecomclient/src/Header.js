import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import Carts from './Carts';
import ProductList from './ProductList';

function Header() {
  return (
    <Router>
      <div>
        <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom shadow-sm">
            <h5 className="my-0 mr-md-auto font-weight-normal text-white">
                <Link className="p-2 text-white" to="/">Ecom</Link>
            </h5>
            <nav className="my-2 my-md-0 mr-md-3">
                <Link className="p-2 text-white" to="/cart">Cart</Link>
            </nav>
            {/* <a className="nav-link btn btn-outline-primary" href="#" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                UserName
            </a>
            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="#">Cart</a>
                <a className="dropdown-item" href="#">Link</a>
                <a className="dropdown-item" href="#">Link</a>
            </div> */}
        </div>

        <Switch>
          <Route path="/cart">
            <div className="container">
                <Carts />
            </div>
          </Route>
          <Route path="/">
          <div className="container">
                <ProductList />
          </div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default Header;
