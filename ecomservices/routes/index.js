const express = require('express');
const router = express.Router();

const products = [{
  id: 1,
  name: 'nikon d5600',
  price: '5600',
  img: 'https://images-na.ssl-images-amazon.com/images/I/51DE4FICRGL.jpg'
},{
  id: 2,
  name: 'nikon 7500',
  price: '15600',
  img: 'https://jacarantastudio.shop/wp-content/uploads/2018/10/nikon_d7500_dslr_camera_with_1492611670000_1333200.jpg'
},{
  id: 3,
  name: 'nikon d7800',
  price: '5600',
  img: 'https://images-na.ssl-images-amazon.com/images/I/41sYYQ%2BEgAL._SX466_.jpg'
},{
  id: 4,
  name: 'nikon d850',
  price: '5600',
  img: 'https://image2.pricedekho.com/p/2/6/76/3412876/27150212-nikon-d850-dslr-camera-24-120-mm-vr-lensblack-picture-large.jpg'
}];

//GET /products
router.get('/products', (req, res, next) => {
  if (products.length < 1) {
    return res.status(404).json({ message: "products not found" });
  }
  res.json({ products: products });
});

//POST /products
router.post('/products', (req, res, next) => {
  let product = req.body;
  product['id'] = products.length+1;
  products.push(product);
  res.json({ product: products });
});

//GET /products/:id
router.get('/products/:id', (req, res, next) => {
  let productId = req.params.id;
  products.forEach(item => {
    if(item.id == productId) {
      res.json({ product: item })
    }
  });
  return res.status(404).json({ message: "product not found" });
});

module.exports = router;
