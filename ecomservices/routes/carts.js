const express = require('express');
const router = express.Router();

const carts = [];

//GET /
router.get('/', (req, res, next) => {
  res.json({
     carts: carts,
     noOfIteams: carts.length
  });
});

//POST /
router.post('/', (req, res, next) => {
  let { cartItem } = req.body;
  console.log(cartItem);
  cartItem['id'] = carts.length+1;
  carts.push(cartItem);
  res.json({
    carts: carts,
    noOfIteams: carts.length
  });
});

//GET /:id
router.get('/:id', (req, res, next) => {
  let cartItemId = req.params.id;
  carts.forEach(item => {
    if(item.id == cartItemId) {
      res.json({ cart: item });
    }
  });
  return res.status(404).json({ message: "product not found" });
});

module.exports = router;
